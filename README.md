# CKA-CKAD Training

## Only imperative mode

- make a configmap from literals
- make a configmap from an envfile
- make a configmap from a file
- make a secret from literals
- make a secret from an envfile
- make a secret from a file
- make a pod with a radial/busyboxplus:curl image
- make a pod with a busybox image and a `sleep 1d` command
- make a pod with a busybox image and a label
- change the label with `kubectl set`
- change the cpu and memory resources with `set`
- make a deployment with an nginx image
- 

## Without documentation

Subrequirements:
- as much as possible, only in imperative mode
- if necessary, change namespace with `k config set-context --current --namespace mynamespace`
- __no documentation__ at kubernetes.io, at most `k explain (--recursive) pods.spec...`

### Configmaps and secrets

- make a configmap with values `var1=val1` and `var2=val2` from literals, with values `var3=val3` and `var4=val4` from envfile, and from file config.txt (filled with random text on several lines).
- make the same but in a secret

### Persistent volume

- make a pv with hostPath `/data` and 100Mi storage, both `RWO` and `RWX`
- make a pvc for this pv and `RWO`

### Pods

- make a pod with: 
    - variable `MANUAL` and value `text variable`
    - all the pre-defined environment variables (pod name, node name, cpu request/limit, memory...)
    - variables `var1` and `var2` loaded with env (same variable names)
    - variables `var3` and `var4` loaded with envFrom (different variable names)
    - config file `config.txt` loaded with a volume mount
    - same with the secrets
    - map the pvc at `/data`
    - three containers:
        - container `c1` (busybox) writing the date to `/data/log.txt`
        - container `c2` (busybox) writing the content in `/data/log.txt` to stdout
        - container `c3` (nginx) with `/data` mapped at `/usr/share/nginx/html`
    - check the logs and curl the nginx container
    - change the cpu and memory requests with `kubectl set`, and check the variables
- make two pods in different namespaces and a network policy so that only them can communicate with each other

### Deployments and services

- make a blue deployment with an nginx image that writes the name of the **pod** in `/usr/share/nginx/html/index.html` (and the word "blue" too). Use other port numbers than the default usual ones, different for the container, the deployment, and the service.
- expose it with a NodePort
- change the image with `k set`
- rollback with `k rollout`
- make a blue/green deployment rollout (with the name of the **node** and the word "green" in file `/usr/share/nginx/html/index.html`)
- turn back with a blue (90%) and green (10%) canary deployment
- curl the deployments (or rather service) with a `radial/busyboxplus:curl` pod in a while loop

### Jobs and cronjobs

- make a job that prints the date in a persistant volume file (replace the previous pv and pvc)
- make a cronjob that does the same every minute and `watch` the file
- add completions, parallism, backoffLimit, activeDeadlineSeconds, and startingDeadlineSeconds (trap with later)

### Limitrange

- make a namespace with a limitrange (cpu request and limit, memory request and limit, number of pods)
- remove both

### Helm

- add the bitnami repo
- install an nginx chart
- uninstall the chart
- remove bitnami
- list the repos


## With documentation

On these ones, you can have a look at the docs

### Role and rolebindings

- make a role and rolebinding to allow myuser to get pods only
- make a crt request for myuser
- make a `~/.kube/config` file for myuser

### Docker

- build the image:
```
FROM alpine
CMD echo "Hello, world"
```
- run a local registry
- push the image to the local registry
- run the image in a pod


---
---

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/thomasbb/cka-ckad-training.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/thomasbb/cka-ckad-training/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
